package com.astra.movic.rental;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.KafkaListener;

@SpringBootApplication
public class RentalApplication {

	public static void main(String[] args) {
		SpringApplication.run(RentalApplication.class, args);
	}

	@KafkaListener(id = "kafka", topics = "pesan")
	public void dltListen(String in) {
		System.out.println("Data yang ditarik : " + in);
	}

}
