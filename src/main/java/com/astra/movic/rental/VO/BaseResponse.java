package com.astra.movic.rental.VO;

// import com.astra.movic.api.config.ConstantConfig;
// import com.astra.movic.api.util.AESenc;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import lombok.Data;

public class BaseResponse<T> {
    // private MetaInfo meta;
    private String message;
    public Object data;

    public BaseResponse() {
        super();
    }

    public void setBaseResponse(int total, int offset, int limit, String message, Object data) {
        MetaInfo metaInfo = new MetaInfo(total, offset, limit);
        // this.meta = metaInfo;
        this.message = message;
        // setData(data);
        this.data = data;
    }

    public Object getData() {
        return data;
    }

    // public void setData(Object data) {
    //     try {
    //         if (data == null || !ConstantConfig.getInstance().getEncrypt()) {
    //             this.data = data;
    //         } else {
    //             ObjectMapper om = new ObjectMapper();
    //             om.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
    //             om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    //             this.data = AESenc.encrypt(om.writeValueAsString(data));
    //         }
    //     } catch (Exception e) {
    //         this.data = data;
    //     }
    // }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    // public MetaInfo getMeta() {
    //     return meta;
    // }

    // public void setMeta(int total, int offset, int limit) {
    //     this.meta = new MetaInfo(total, offset, limit);
    // }
}

@Data
class MetaInfo {
    private int total;
    private int offset;
    private int limit;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public MetaInfo(int total, int offset, int limit) {
        super();
        this.total = total;
        this.offset = offset;
        this.limit = limit;
    }

}