package com.astra.movic.rental.VO;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class MapProfile {

    @JsonProperty("access_token")
    @SerializedName("access_token")
    private String accessToken;

    @JsonProperty("expired_date")
    @SerializedName("expired_date")
    private Date expiredDate;

    @JsonProperty("user_type")
    @SerializedName("user_type")
    private String userType;

    @JsonProperty("profile_data")
    @SerializedName("profile_data")
    private ProfileData profileData;

    private String features;
}