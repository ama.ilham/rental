package com.astra.movic.rental.VO;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class ProfileData{

    private String id;

    @JsonProperty("first_name")
    @SerializedName("first_name")
    private String firstName;
    @JsonProperty("last_name")
    @SerializedName("last_name")
    private String lastName;

    private String email;
    private String phone;
}