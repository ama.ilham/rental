package com.astra.movic.rental.Controller;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import com.astra.movic.rental.VO.BaseResponse;
import com.astra.movic.rental.VO.MapProfile;
import com.astra.movic.rental.util.AESenc;
import com.google.gson.Gson;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.requestreply.ReplyingKafkaTemplate;
import org.springframework.kafka.requestreply.RequestReplyFuture;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.kafka.support.SendResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("hello")
public class HelloUserController {

    // @Autowired
    // private KafkaTemplate<Object, Object> template;

    @Autowired
    private ReplyingKafkaTemplate<String, Object, Object> replyTemplate;

    @GetMapping("/")
    public ResponseEntity<String> hello(@RequestHeader(value = "api_key") String apiKey,
            @RequestHeader(value = "token") String token) {

        // set content type
        List<MediaType> list = new ArrayList<>();
        list.add(MediaType.APPLICATION_JSON);

        // set header type
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(list);
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("api_key", apiKey);
        headers.set("token", token);

        HttpEntity<String> requestEntity = new HttpEntity<>(null, headers);

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

        String decryptedData = "";
        String message = "";
        HttpStatus status = HttpStatus.OK;
        String url = "http://localhost:3003/profile/";

        ResponseEntity<BaseResponse> response = null;

        try {
            for (int i = 0; i < 5; i++) {
                response = restTemplate.exchange(url, HttpMethod.POST, requestEntity, BaseResponse.class);
                decryptedData = AESenc.decrypt(response.getBody().getData().toString());

                Gson gson = new Gson();
                MapProfile profile = gson.fromJson(decryptedData, MapProfile.class);
                message = "Hello, " + profile.getProfileData().getFirstName();
            }
        } catch (Exception e) {
            // TODO: handle exception
            message = "You are Unauthorized";
            status = HttpStatus.UNAUTHORIZED;
        }

        ResponseEntity<String> greeting = new ResponseEntity<String>(message, status);

        return greeting;
    }

    @GetMapping("/kafka")
    public ResponseEntity<String> kafkaEx(
        @RequestHeader(value = "api_key") String apiKey,
        @RequestHeader(value = "token") String token) 
        throws InterruptedException, ExecutionException {
        
            ProducerRecord<String, Object> record = new ProducerRecord<String,Object>("pesan", token);
            record.headers().add(new RecordHeader(KafkaHeaders.REPLY_TOPIC, "balasan".getBytes()));
            RequestReplyFuture<String, Object, Object> response = replyTemplate.sendAndReceive(record);

            SendResult<String, Object> sendResult = response.getSendFuture().get();
        
            sendResult.getProducerRecord().headers().forEach(header -> System.out.println(header.key() + ":" + header.value().toString()));
            ConsumerRecord<String, Object> consumerRecord = response.get();
            
            String greeting = "Hello " + consumerRecord.value();
            // for (int i = 0; i < 10; i++) {
            //     this.template.send("pesan", "Pesan ke-" + i);
            // }

            return new ResponseEntity<String>(greeting, HttpStatus.OK);
    }
}